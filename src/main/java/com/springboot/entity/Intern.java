package com.springboot.entity;

import jakarta.persistence.*;

import javax.lang.model.element.Name;

@Entity
@Table(name = "intern_details")
public class Intern {

@Id
@GeneratedValue(strategy= GenerationType.AUTO)
    private long internId;
    private String internName;
    private String internPosition;
    private String internAddress;

    public long getInternId() {
        return internId;
    }

    public void setInternId(long internId) {
        this.internId = internId;
    }

    public String getInternName() {
        return internName;
    }

    public void setInternName(String internName) {
        this.internName = internName;
    }

    public String getInternPosition() {
        return internPosition;
    }

    public void setInternPosition(String internPosition) {
        this.internPosition = internPosition;
    }

    public String getInternAddress() {
        return internAddress;
    }

    public void setInternAddress(String internAddress) {
        this.internAddress = internAddress;
    }

    public Intern(long internId, String internName, String internPosition, String internAddress) {
        this.internId = internId;
        this.internName = internName;
        this.internPosition = internPosition;
        this.internAddress = internAddress;
    }

    public Intern() {

    }

    @Override
    public String toString() {
        return "Intern{" +
                "internId=" + internId +
                ", internName='" + internName + '\'' +
                ", internPosition='" + internPosition + '\'' +
                ", internAddress='" + internAddress + '\'' +
                '}';
    }
}
