package com.springboot.service;

import com.springboot.entity.Intern;

import java.util.List;

public interface InternService {
   public Intern addIntern(Intern intern);

  public List<Intern> getAllIntern();

  public  Intern getInternById(Long internId);

  public  void removeInternById(Long id);

   public Intern updateInternById(Long id, Intern intern);
}
