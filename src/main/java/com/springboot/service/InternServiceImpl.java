package com.springboot.service;

import com.springboot.entity.Intern;
import com.springboot.repository.InternRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class InternServiceImpl implements InternService{

    @Autowired
    private InternRepository internRepository;
    @Override
    public Intern addIntern(Intern intern) {
        return internRepository.save(intern);
    }

    @Override
    public List<Intern> getAllIntern() {
        return internRepository.findAll();
    }

    @Override
    public Intern getInternById(Long internId) {
        return internRepository.findById(internId).get();
    }

    @Override
    public void removeInternById(Long id) {

         internRepository.deleteById(id);
    }

    @Override
    public Intern updateInternById(Long id, Intern intern) {

        Intern intDb =internRepository.findById(id).get();

        if(Objects.nonNull(intern.getInternName()) &&
        !"".equalsIgnoreCase(intern.getInternName()))
        {
            intDb.setInternName(intern.getInternName());
        }

        if(Objects.nonNull(intern.getInternPosition()) &&
                !"".equalsIgnoreCase(intern.getInternPosition()))
        {
            intDb.setInternPosition(intern.getInternPosition());
        }

        if(Objects.nonNull(intern.getInternAddress()) &&
                !"".equalsIgnoreCase(intern.getInternAddress()))
        {
            intDb.setInternAddress(intern.getInternAddress());
        }

        return  internRepository.save(intDb);

    }
}
