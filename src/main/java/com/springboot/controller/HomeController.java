package com.springboot.controller;

import com.springboot.entity.Intern;
import com.springboot.service.InternService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HomeController {
    @Autowired
    private InternService internService;
    @PostMapping("/add")
    public Intern addIntern(@RequestBody Intern intern)
    {
        return  internService.addIntern(intern);
    }

    @GetMapping("/getAll")
    public List<Intern> getAllIntern()
    {
        return internService.getAllIntern();
    }

    @GetMapping("/get/{id}")
    public Intern getInternById(@PathVariable("id") Long internId)
    {
        return internService.getInternById(internId);
    }

    @DeleteMapping("/delete/{id}")
    public String removeInternById(@PathVariable("id") Long id)
    {
        internService.removeInternById(id);

      return  "Removed successfully!!";
    }

    @PutMapping("/update/{id}")
    public Intern updateInternById(@PathVariable("id") Long id ,@RequestBody Intern intern)
    {
       return internService.updateInternById(id,intern);
    }





}
